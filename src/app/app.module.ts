import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './Nav-Bar/Nav-Bar.component';
import { SliderComponent } from './slider/slider.component';
import { ParagraphComponent } from './paragraph/paragraph.component';

@NgModule({
  declarations: [			
    AppComponent,
      NavBarComponent,
      SliderComponent,
      ParagraphComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
